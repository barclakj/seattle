package com.nfa.seattle.rest;

import com.nfa.seattle.controller.SeattleClientTracker;
import com.nfa.seattle.controller.SeattleException;
import com.nfa.seattle.model.Client;
import com.nfa.seattle.model.Project;
import com.nfa.seattle.views.ClientView;
import com.nfa.seattle.views.ProjectView;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;


@Path("/config")
public class SeattleService {
	private static Logger log = Logger.getLogger(SeattleService.class.getCanonicalName());

	public static final String SEATTLE_KEY = "X-SEATTLE-KEY";
	/**
	 * Checks for any changes to configuration and updates the status for the client.
	 * @return
	 */
	@GET
	@Path("/{project}/{uuid}")
	@Produces(MediaType.APPLICATION_JSON)
	public String checkProjectConfig(@PathParam("project") String project,
						  @PathParam("uuid") String uuid,
						  @HeaderParam(SEATTLE_KEY) String key,
						  @Context HttpServletRequest req,
						  @Context HttpServletResponse resp) {
		JSONObject config = null;
		log.info("Loading config for project " + project + " with key " + key);
		try {
			config = SeattleClientTracker.getProjectConfig(project, key);
			if (config == null) {
				// they'll be no properties. t
				config = new JSONObject();
			}
			Client c = SeattleClientTracker.updateClient(project, uuid, req.getRemoteAddr());
			config.put("___client", ClientView.toJSON(c));
			config.put("___project", ProjectView.toJSON(SeattleClientTracker.getProject(project)));
		} catch (SeattleException e) {
			log.warning("Exception loading config data: " + e.getMessage());
			resp.sendError(Response.Status.BAD_REQUEST.getStatusCode(), e.getLocalizedMessage());
		} finally {
			if (config!=null) return config.toString();
			else return null;
		}
	}

	/**
	 * Return list of projects
	 * @return
	 */
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public String listProjects(@Context HttpServletRequest req,
						  @Context HttpServletResponse resp) {

		Collection<Project> projects = SeattleClientTracker.listProjects();

		return ProjectView.toJSON(projects).toString();
	}

	/**
	 * Return details of specific project.
	 * @return
	 */
	@GET
	@Path("/{project}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getProject(@PathParam("project") String project,
							 @HeaderParam(SEATTLE_KEY) String key,
							  @Context HttpServletRequest req,
							   @Context HttpServletResponse resp) {

		return ProjectView.toJSON(SeattleClientTracker.getProject(project)).toString();
	}

	/**
	 * Uploads a new config file.
	 * @return
	 */
	@POST
	@Path("/{project}")
	@Produces(MediaType.APPLICATION_JSON)
	public String uploadProjectConfig(@PathParam("project") String project,
							  @HeaderParam(SEATTLE_KEY) String key,
							 @Context HttpServletRequest req,
							 @Context HttpServletResponse resp) {
		String response = null;
		try {

			String body = getBodyAsString(req);
			try {
				JSONObject jo = new JSONObject(body);
				boolean success = SeattleClientTracker.uploadNewProject(project, jo, key);
				if (!success) {
					resp.sendError(500, "Unable to save data!");
					response = "Unable to save data!";
				} else {
					response = ProjectView.toJSON(SeattleClientTracker.getProject(project)).toString();
				}
			} catch (JSONException e) {
				// its not json!
				resp.sendError(400, "Config data is not JSON!");
				response = "Config data is not JSON!";
			}
		} catch (IOException e) {
			log.log(Level.SEVERE, "Exception processing config upload for '" + project + "': " + e.getMessage(), e);
		} finally {
			return response;
		}
	}

	/**
	 * Updates an existing config file (only if the key matches).
	 * @return
	 */
	@PUT
	@Path("/{project}")
	@Produces(MediaType.APPLICATION_JSON)
	public String updateProjectConfig(@PathParam("project") String project,
									  @HeaderParam(SEATTLE_KEY) String key,
									  @Context HttpServletRequest req,
									  @Context HttpServletResponse resp) {
		String response = null;
		try {
			String body = getBodyAsString(req);
			try {
				JSONObject jo = new JSONObject(body);
				boolean success = SeattleClientTracker.updateExistingProject(project, jo, key);
				if (!success) {
					resp.sendError(500, "Unable to save data!");
					response = "Unable to save data!";
				} else {
					response = ProjectView.toJSON(SeattleClientTracker.getProject(project)).toString();
				}
			} catch (JSONException e) {
				// its not json!
				resp.sendError(400, "Config data is not JSON!");
				response = "Config data is not JSON!";
			}
		} catch (IOException e) {
			log.log(Level.SEVERE, "Exception processing config update for '" + project + "': " + e.getMessage(), e);
		} finally {
			return response;
		}
	}

	/**
	 * Deletes a project and config file (only if the key matches).
	 * @return
	 */
	@DELETE
	@Path("/{project}")
	@Produces(MediaType.APPLICATION_JSON)
	public String deleteProjectConfig(@PathParam("project") String project,
									  @HeaderParam(SEATTLE_KEY) String key,
									  @Context HttpServletRequest req,
									  @Context HttpServletResponse resp) {
		String response = null;
		try {
			boolean success = SeattleClientTracker.deleteProject(project, key);
			if (!success) {
				resp.sendError(500, "Unable to delete project!");
				response = "Unable to save data!";
			} else {
				response = ProjectView.toJSON(SeattleClientTracker.getProject(project)).toString();
			}
		} catch (IOException e) {
			log.log(Level.SEVERE, "Exception processing config delete for '" + project + "': " + e.getMessage(), e);
		} finally {
			return response;
		}
	}

	/**
	 * Returns the body of a request as a string.
	 * @param req
	 * @return
	 */
	private static String getBodyAsString(HttpServletRequest req) {
		StringBuilder b = new StringBuilder();
		try {
			BufferedReader r = req.getReader();
			String line = null;
			while ((line = r.readLine()) != null) {
				b.append(line);
			}
			if (log.isLoggable(Level.FINE))
				log.fine("Body: " + b.toString());
		} finally {
			return b.toString();
		}
	}
}