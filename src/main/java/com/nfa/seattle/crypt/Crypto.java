package com.nfa.seattle.crypt;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.*;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by barclakj on 10/06/2017.
 */
public class Crypto {
    public static Logger log = Logger.getLogger(Crypto.class.getCanonicalName());

    public static String CIPHER_NAME = "AES/CTR/NoPadding";

    public static final int DEFAULT_KEY_SIZE_BITS = 128;

    private static int AES_KEYLENGTH = 128;
    private static int ivLengthBytes = AES_KEYLENGTH / 8;  // Save the IV bytes or send it in plaintext with the encrypted data so you can decrypt the data later

    public static Key keyFromBytes(byte[] keyData) {
        byte[] key = Arrays.copyOf(keyData, DEFAULT_KEY_SIZE_BITS/8);
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        return secretKeySpec;
    }

    public static byte[] generateKey(int size) {
        SecureRandom sr = new SecureRandom();
        sr.setSeed(System.currentTimeMillis() + (long)(Math.random()*Integer.MAX_VALUE));
        byte[] bytes = new byte[size/8];
        sr.nextBytes(bytes);
        return bytes;
    }

    private static Map<Integer, String> HASH_ALGS = new HashMap<>();

    static {
        HASH_ALGS.put(128, "MD5");
        HASH_ALGS.put(256, "SHA-256");
    }

    public static byte[] b64decode(String encdata) {
        byte[] data = Base64.getDecoder().decode(encdata);
        return data;
    }

    public static String b64encode(byte[] data) {
        byte[] encdata = Base64.getEncoder().encode(data);
        return new String(encdata);
    }

    public static final String PREPENDBYTES = "T133";


    public static void encrypt(InputStream is, OutputStream os, byte[] key) throws CryptException {
        int totalRead = 0;
        Key k = keyFromBytes(key);

        byte[] iv = Arrays.copyOfRange(key, 0, ivLengthBytes);

        try {
            Cipher aesCipherForEncryption = Cipher.getInstance(CIPHER_NAME);
            aesCipherForEncryption.init(Cipher.ENCRYPT_MODE, k, new IvParameterSpec(iv));

            CipherOutputStream cryptOut = new CipherOutputStream(new BufferedOutputStream(os), aesCipherForEncryption);
            log.fine("Encrypting...");
            cryptOut.write(PREPENDBYTES.getBytes(), 0, PREPENDBYTES.length()); // write the prepend bytes for later valdiation.

            byte[] data = new byte[8192];

            int read = 0;
            while((read = is.read(data, 0, 8192)) > 0) {
                cryptOut.write(data,0, read);
                totalRead+=read;
            }
            cryptOut.flush();
        } catch (InvalidAlgorithmParameterException e) {
            log.log(Level.WARNING, e.getClass().getCanonicalName() + " " + e.getMessage(), e);
            throw new CryptException(e);
        } catch (InvalidKeyException e) {
            log.log(Level.WARNING, e.getClass().getCanonicalName() + " " + e.getMessage(), e);
            throw new CryptException(e);
        } catch (NoSuchPaddingException e) {
            log.log(Level.WARNING, e.getClass().getCanonicalName() + " " + e.getMessage(), e);
            throw new CryptException(e);
        } catch (NoSuchAlgorithmException e ) {
            log.log(Level.WARNING, e.getClass().getCanonicalName() + " " + e.getMessage(), e);
            throw new CryptException(e);
        } catch (IOException e ) {
            log.log(Level.WARNING, e.getClass().getCanonicalName() + " " + e.getMessage(), e);
            throw new CryptException(e);
        } finally {
            log.fine("Total encrypted bytes (exc. prepend): " + totalRead + " bytes");
        }
    }

    public static void decrypt(InputStream is, OutputStream os, byte[] key) throws CryptException {
        Key k = keyFromBytes(key);
        int totalRead = 0;

        byte[] iv = Arrays.copyOfRange(key, 0, ivLengthBytes);

        try {
            Cipher aesCipherForEncryption = Cipher.getInstance(CIPHER_NAME);
            aesCipherForEncryption.init(Cipher.DECRYPT_MODE, k, new IvParameterSpec(iv));

            CipherInputStream cryptIn = new CipherInputStream(new BufferedInputStream(is), aesCipherForEncryption);
            log.fine("Decrypting...");

            boolean prependChecked = false;
            byte[] data = new byte[8192];
            int read = 0;
            while((read = cryptIn.read(data, 0, 8192)) > 0) {
                if (!prependChecked) {
                    String prepend = new String(data,0, PREPENDBYTES.length());
                    if (!prepend.equals(PREPENDBYTES)) {
                        // not ok
                        log.warning("Incorrect prepend key match: " + prepend);
                        throw new CryptException("Incorrect key");
                    } else {
                        os.write(data, PREPENDBYTES.length(), read-PREPENDBYTES.length());
                        prependChecked = true;
                        totalRead+=(read-PREPENDBYTES.length());
                    }
                } else {
                    os.write(data, 0, read);
                    totalRead += read;
                }
            }
            // os.flush();

        } catch (InvalidAlgorithmParameterException e) {
            log.log(Level.WARNING, e.getClass().getCanonicalName() + " " + e.getMessage(), e);
            throw new CryptException(e);
        } catch (InvalidKeyException e) {
            log.log(Level.WARNING, e.getClass().getCanonicalName() + " " + e.getMessage(), e);
            throw new CryptException(e);
        } catch (NoSuchPaddingException e) {
            log.log(Level.WARNING, e.getClass().getCanonicalName() + " " + e.getMessage(), e);
            throw new CryptException(e);
        } catch (NoSuchAlgorithmException e ) {
            log.log(Level.WARNING, e.getClass().getCanonicalName() + " " + e.getMessage(), e);
            throw new CryptException(e);
        } catch (IOException e ) {
            log.log(Level.WARNING, e.getClass().getCanonicalName() + " " + e.getMessage(), e);
            throw new CryptException(e);
        } finally {
            log.fine("Total decrypted bytes (exc. prepend): " + totalRead + " bytes");
        }
    }

}
