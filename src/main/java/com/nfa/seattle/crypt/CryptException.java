package com.nfa.seattle.crypt;

/**
 * Created by barclakj on 10/06/2017.
 */
public class CryptException extends Throwable {

    public CryptException(Throwable t) {
        super(t);
    }

    public CryptException(String msg) {
        super(msg);
    }

}
