package com.nfa.seattle.s3;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.S3ClientOptions;
import com.amazonaws.services.s3.model.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by barclakj on 12/11/2017.
 */
public class S3Store {
    private static Logger log = Logger.getLogger(S3Store.class.getCanonicalName());

    private String accessKey = null;
    private String secretKey = null;
    private String endpoint = null;
    private String bucketName = null;

    private AmazonS3Client client = null;

    public void connect() {
        ClientConfiguration cc = new ClientConfiguration();
        cc.setSignerOverride("S3SignerType");                                      // Change auth protocol
        BasicAWSCredentials creds = new BasicAWSCredentials(accessKey, secretKey);
        client = new AmazonS3Client(creds, cc);
        client.setEndpoint(endpoint);
        S3ClientOptions options = new S3ClientOptions();
        options.setPathStyleAccess(true);
        client.setS3ClientOptions(options);
        log.info("Connected to S3 " + this.endpoint + " Using bucket: " + this.bucketName);
    }

    public void putObject(String objectName, InputStream is, long size)  { // throws NoSuchAlgorithmException {
        if (client==null) connect();
        log.fine("Putting object: " + objectName + " to bucket " + bucketName);
        long start = System.nanoTime();
        ObjectMetadata md = new ObjectMetadata();
        md.setContentType("application/octet-stream");
        md.setLastModified(new Date());
        md.setContentLength(size);

        PutObjectResult por = client.putObject(bucketName, objectName, is, md);

        long end = System.nanoTime();
        log.fine("putObject - Duration-ms: " + (end-start) + "ns");
    }

    public long getObjectSize(String objectName) {
        long start = System.nanoTime();
        if (client==null) connect();
        log.fine("GetObjectSize - Fetching.. " + objectName + " in bucket " + bucketName);
        ObjectMetadata md = client.getObjectMetadata(bucketName, objectName);
        long size = md.getContentLength();
        long end = System.nanoTime();
        log.fine("GetObjectSize - Duration-ms: " + (end-start) + "ns");
        return size;
    }

    public InputStream getObject(String objectName) {
        long start = System.nanoTime();
        if (client==null) connect();
        log.fine("GetObject - Fetching.. " + objectName + " in bucket " + bucketName);
        S3Object s3o = client.getObject(bucketName, objectName);
        InputStream is = null;
        is = s3o.getObjectContent();
        long end = System.nanoTime();
        log.fine("GetObject - Duration-ms: " + (end-start) + "ns");
        return is;
    }

    public void deleteObject(String objectName) {
        long start = System.nanoTime();
        if (client==null) connect();
        log.fine("DeleteObject - Deleting.. " + objectName + " in bucket " + bucketName);
        client.deleteObject(bucketName, objectName);
        long end = System.nanoTime();
        log.fine("DeleteObject - Duration-ms: " + (end-start) + "ns");
    }

    public void renameObject(String objectName, String newObjectName) {
        long start = System.nanoTime();
        if (client==null) connect();
        log.fine("RenameObject - Renaming.. " + objectName + " in bucket " + bucketName);
        CopyObjectResult copyResult = client.copyObject(bucketName, objectName, bucketName, newObjectName);
        deleteObject(objectName);
        long end = System.nanoTime();
        log.fine("RenameObject - Duration-ms: " + (end-start) + "ns");
    }

    private static String LOCK_SUFFIX = ".lock";

    public boolean lockObject(String objectName, String signature) {
        long start = System.nanoTime();
        boolean locked = false;
        if (client==null) connect();
        log.fine("LockObject: " + objectName + " in bucket " + bucketName);
        if (client.doesObjectExist(bucketName, objectName + LOCK_SUFFIX)) {
            // cannot obtain lock but is it mine?
            S3Object s3o = client.getObject(bucketName, objectName + LOCK_SUFFIX);
            try {
                String data = inputStreamToString(s3o.getObjectContent());
                if (data.equals(signature)) {
                    // its my lock!
                    locked = true;
                }
            } catch (IOException e) {
                log.log(Level.WARNING, "Unable to read lock file data so cannot release lock!" + e.getMessage(), e);
            }
        } else {
            ObjectMetadata om = new ObjectMetadata();
            om.setContentType("text/plain");
            om.setContentLength(signature.length());
            // om.setExpirationTime(); // can we set expirty here for like + 5 mins to prevent infinite lock on some failure?
            PutObjectResult putResult = client.putObject(bucketName, objectName + LOCK_SUFFIX, new ByteArrayInputStream(signature.getBytes()), om);
            locked = true;
        }
        long end = System.nanoTime();
        log.fine("LockObject - Duration-ms: " + (end-start) + "ns");
        return locked;
    }

    public boolean checkExists(String objectName) {
        return client.doesObjectExist(bucketName, objectName);
    }

    public boolean releaseObjectLock(String objectName, String signature) {
        long start = System.nanoTime();
        boolean unlocked = false;
        if (client==null) connect();
        log.fine("UnlockObject: " + objectName + " in bucket " + bucketName);
        if (client.doesObjectExist(bucketName, objectName + LOCK_SUFFIX)) {
            // theres a lock... should be mine.
            S3Object s3o = client.getObject(bucketName, objectName + LOCK_SUFFIX);

            try {
                String data = inputStreamToString(s3o.getObjectContent());
                if (data.equals(signature)) {
                    // its my lock!
                    deleteObject(objectName + LOCK_SUFFIX);
                    unlocked = true;
                }
            } catch (IOException e) {
                log.log(Level.WARNING, "Unable to read lock file data so cannot release lock!" + e.getMessage(), e);
            }
        }
        long end = System.nanoTime();
        log.fine("UnlockObject - Duration-ms: " + (end-start) + "ns");
        return unlocked;
    }

    public static String inputStreamToString(InputStream is) throws IOException {
        byte[] bytes = inputStreamToBytes(is);
        return new String(bytes,"UTF-8");
    }

    public static byte[] inputStreamToBytes(InputStream is) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int size = 8192;
        byte[] data = new byte[size];
        int read =0;
        while ((read=is.read(data,0,8192))>0) {
            baos.write(data,0, read);
        }
        is.close();
        return baos.toByteArray();
    }

    private static final String NEW_OBJECT_SUFFIX = ".new";

    public boolean replaceObject(String objectName, InputStream is, int size, String signature) {
        // lock
        boolean replaced = false;
        boolean locked = lockObject(objectName, signature);
        try {
            if (locked) {
                // create new
                putObject(objectName + NEW_OBJECT_SUFFIX, is, size);
                // delete old if exists
                if (client.doesObjectExist(bucketName, objectName)) {
                    deleteObject(objectName);
                }
                // rename new
                renameObject(objectName + NEW_OBJECT_SUFFIX, objectName);
                // unlock
                releaseObjectLock(objectName, signature);
                replaced = true;
            }
        } finally {
            if (locked) {
                releaseObjectLock(objectName, signature);
            }
        }
        return replaced;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }
}
