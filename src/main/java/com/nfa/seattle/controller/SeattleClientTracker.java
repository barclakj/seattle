package com.nfa.seattle.controller;

import com.amazonaws.auth.policy.PolicyReaderOptions;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.nfa.seattle.crypt.CryptException;
import com.nfa.seattle.crypt.Crypto;
import com.nfa.seattle.model.Client;
import com.nfa.seattle.model.Project;
import com.nfa.seattle.s3.S3Store;
import com.nfa.seattle.views.ProjectView;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Main controller for Seattle.
 *
 * Created by barclakj on 10/02/2018.
 */
public class SeattleClientTracker {
    private static Logger log = Logger.getLogger(SeattleClientTracker.class.getCanonicalName());

    private static Map<String, Project> allProjects = new HashMap<>();

    private static final long MAX_CLIENT_AGE = 90000; // 90 seconds

    private static final long MAX_CONFIG_AGE = 90000; // 90 seconds

    private static SeattleScheduler scheduler = new SeattleScheduler();

    private static S3Store store = null;

    public static final String PROJECT_CONFIG_FILE = "seattle-projects.json";

    private static final String SIGNATURE = UUID.randomUUID().toString();

    static {
        String accessKey = System.getenv("ACCESS_KEY");
        String secret = System.getenv("SECRET_KEY");
        String bucketName = System.getenv("BUCKET_NAME");
        String endpoint = System.getenv("ENDPOINT");

        store = new S3Store();
        store.setAccessKey(accessKey);
        store.setEndpoint(endpoint);
        store.setSecretKey(secret);
        store.setBucketName(bucketName);
        store.connect();

        loadConfig();
    }

    /**
     * Updates the client timestamp for the specified project and uuid.
     * If the project doesn't exist, creates it.
     * If the client doesn't exist the create it.
     * Needs to be sync'ed across all threads and nodes hence use of locks
     * and synchronization.
     * Returns the client.
     * @param project
     * @param uuid
     * @param address
     */
    public static synchronized Client updateClient(String project, String uuid, String address) {
        Client c = null;
        synchronized (allProjects) {
            boolean islocked = store.lockObject(PROJECT_CONFIG_FILE, SIGNATURE); // get lock
            if (islocked) {
                loadConfig(); // reload config
                Project p = getOrCreateProject(project);
                c = p.getClient(uuid);
                if (c == null) {
                    log.info("Create client: " + uuid + " " + address);
                    c = new Client();
                    c.setUuid(uuid);
                    c.setId(p.getNextId());
                    c.setAddress(address);
                    c.setCreatedTs(System.currentTimeMillis());
                    p.addClient(uuid, c);
                }
                p.ping(uuid);
                // save config
                String data = ProjectView.toJSON(allProjects.values()).toString();
                store.replaceObject(PROJECT_CONFIG_FILE, new ByteArrayInputStream(data.getBytes()), data.length(), SIGNATURE);
            }
        }
        return c;
    }

    private static void loadConfig() {
        try {
            InputStream is = store.getObject(PROJECT_CONFIG_FILE);
            String data = S3Store.inputStreamToString(is);
            Map<String, Project> projects = ProjectView.toProjectMap(data);
            synchronized (allProjects) {
                // switch them over
                allProjects = projects;
            }
        } catch (AmazonS3Exception e) {
            if (e.getMessage().indexOf("NoSuchKey")>=0) {
                // this is ok.
            } else {
                log.log(Level.SEVERE, e.getMessage(), e);
            }
        } catch (IOException e) {
            // unable to reload config!
            log.log(Level.SEVERE, "Unable to reload config: " + e.getMessage(), e);
        }
    }

    /**
     * Returns handle on a project.
     * @param project
     * @return
     */
    public static Project getProject(String project) {
        return( allProjects.get(project) );
    }

    /**
     * Returns list of projects.
     * @return
     */
    public static Set<String> listProjectNames() {
        return allProjects.keySet();
    }

    /**
     * Get project collection.
     * @return
     */
    public static Collection<Project> listProjects() {
        return allProjects.values();
    }

    /**
     * Returns project by name. Creates it if it doesn't exist.
     * @param project
     * @return
     */
    private static synchronized Project getOrCreateProject(String project) {
        Project p = allProjects.get(project);
        if (p==null) {
            synchronized (allProjects) {
                log.info("Creating new project: " + project);
                p = new Project();
                p.setProjectName(project);
                allProjects.put(project, p);
            }
        }
        return p;
    }

    /**
     * Removes a project from the collection.
     * @param project
     */
    private static synchronized void remProject(String project) {
        log.info("Removing project: " + project);
        synchronized (allProjects) {
            allProjects.remove(project);
        }
    }

    /**
     * Cleans up projects to remove dead clients.
     * If there are no clients for a project then
     * the project is also removed.
     */
    public static synchronized void cleanupProjects() {
        boolean islocked = store.lockObject(PROJECT_CONFIG_FILE, SIGNATURE); // get lock

        if (islocked) {
            loadConfig();
            allProjects.values().parallelStream()
                    .forEach(p -> {
                        long oldestAllowed = System.currentTimeMillis() - MAX_CLIENT_AGE;

                        Set<Client> remClients = p.getClients().stream()
                                .filter(c -> c.getTs() < oldestAllowed)
                                .collect(Collectors.toSet());

                        log.info("Identified " + remClients.size() + " dead clients for project: " + p.getProjectName());

                        remClients.stream()
                                .forEach(c -> p.removeClient(c.getUuid()));
                    });

            Set<String> remProjects = allProjects.keySet().stream()
                    .filter(n -> getProject(n).getClients().size() == 0)
                    .collect(Collectors.toSet());

            remProjects.stream()
                    .forEach(n -> remProject(n));

            String data = ProjectView.toJSON(allProjects.values()).toString();
            store.replaceObject(PROJECT_CONFIG_FILE, new ByteArrayInputStream(data.getBytes()), data.length(), SIGNATURE);
        }
    }

    private static final String PROJECT_CONFIG_SUFFIX = ".json";

    /**
     * Returns the project config for the specified project.
     * Decrypts the data if key provided.
     * Only loads the data from storage if it has aged more than max allowed.
     * @param projectname
     * @param key
     * @return
     */
    public static JSONObject getProjectConfig(String projectname, String key) throws SeattleException {
        boolean newData = false;

        Project p = getOrCreateProject(projectname);
        String name = projectname + PROJECT_CONFIG_SUFFIX;
        if (p.getLastLoadTs()<System.currentTimeMillis()-MAX_CONFIG_AGE) {
            try {
                InputStream is = store.getObject(name);
                byte[] data = S3Store.inputStreamToBytes(is);

                p.setConfigData(data);
                p.setLastLoadTs(System.currentTimeMillis());
                newData = true;
            } catch (IOException e) {
                log.warning("IOException - cannot read. Will use old data if we have it: " + e.getMessage());
                // no need to throw a client exception
            } catch (AmazonS3Exception e) {
                log.warning("AWS exception - missing config?: " + e.getMessage());
                // no need to throw a client exception
            }
        }

        JSONObject jo = decryptConfigData(p, key);
        if (newData && jo==null) {
            // can't trust the data we have if we have any.
            p.setLastLoadTs(0);
        }

        return jo;
    }

    /**
     * Decrypt project byte array data and converts to JSON.
     * Null if an exception occurs.
     * @param p
     * @param key
     * @return
     */
    private static JSONObject decryptConfigData(Project p, String key) throws SeattleException {
        JSONObject jo = null;
        try {
            if (p!=null && p.getConfigData()!=null) {
                if (key != null) {
                    ByteArrayOutputStream os = new ByteArrayOutputStream();
                    ByteArrayInputStream cis = new ByteArrayInputStream(p.getConfigData());

                    Crypto.decrypt(cis, os, key.getBytes());

                    String decData = new String(os.toByteArray(), "UTF-8");
                    jo = new JSONObject(decData);
                } else {
                    // not encyrpted - try raw!
                    jo = new JSONObject(new String(p.getConfigData(), "UTF-8"));
                }
            } // uh oh. no data.. it'll be a null.
        } catch (CryptException e) {
            // wrong key?
            log.warning("Crypt exception - likely wrong key: " + e.getMessage());
            throw new SeattleException(e);
        } catch (UnsupportedEncodingException e) {
            log.warning("Encoding exception - wrong charset?: " + e.getMessage());
            throw new SeattleException(e);
        } catch (JSONException e) {
            log.warning("Doesnt look like JSON - wrong key?: " + e.getMessage());
            throw new SeattleException(e);
        } finally {
            return jo;
        }
    }

    /**
     * Uploads a new data to storage and encrypts if needed.
     * @param project
     * @param jo
     * @param key
     * @return
     */
    public static boolean uploadNewProject(String project, JSONObject jo, String key) throws SeattleException {
        boolean success = false;
        String name = project + PROJECT_CONFIG_SUFFIX;

        if (store.checkExists(name)) {
            // exists - cannot create new
            throw new SeattleException("Project " + project + " already exists. Update or delete first!");
        } else {

            byte[] data = encryptConfigData(jo, key);

            success = store.replaceObject(
                    project + PROJECT_CONFIG_SUFFIX,
                    new ByteArrayInputStream(data),
                    data.length,
                    SIGNATURE);
        }
        return success;
    }

    /**
     * Updates an existing project config.
     * @param project
     * @param jo
     * @param key
     * @return
     */
    public static boolean updateExistingProject(String project, JSONObject jo, String key) throws SeattleException {
        boolean success = false;
        String name = project + PROJECT_CONFIG_SUFFIX;

        JSONObject joFound = getProjectConfig(project, key);
        if (joFound!=null) {
            // can only replace if found.
            byte[] data = encryptConfigData(jo, key);

            success = store.replaceObject(
                    project + PROJECT_CONFIG_SUFFIX,
                    new ByteArrayInputStream(data),
                    data.length,
                    SIGNATURE);
        } else {
            throw new SeattleException("Project or key mismatch. Cannot update. Project: " + project);
        }
        return success;
    }

    /**
     * Deletes a project, but only if the key works!.
     * @param project
     * @param key
     * @return
     */
    public static boolean deleteProject(String project, String key) throws SeattleException {
        boolean success = false;

        JSONObject jo = getProjectConfig(project, key);
        if (jo==null) {
            // not a valid key...
            throw new SeattleException("Project does not exist or key mismatch! Project: " + project);
        } else {
            String name = project + PROJECT_CONFIG_SUFFIX;
            store.deleteObject(name);
            remProject(project);
            success = true;
        }
        return success;
    }

    /**
     * Encrypts project config JSON so long as key provided.
     * Null if an exception occurs.
     * @param jo
     * @param key
     * @return
     */
    private static byte[] encryptConfigData(JSONObject jo, String key) throws SeattleException {
        byte[] encData = null;
        String data = jo.toString();
        try {
            if (key != null) {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                ByteArrayInputStream cis = new ByteArrayInputStream(data.getBytes());

                Crypto.encrypt(cis, os, key.getBytes());

                encData = os.toByteArray();
            } else {
                // not to be encrypted - store raw!
                encData = data.getBytes();
            }
        } catch (CryptException e) {
            // wrong key?
            log.warning("Crypt exception - likely wrong key: " + e.getMessage());
            throw new SeattleException(e);
        } catch (JSONException e) {
            log.warning("Doesnt look like JSON - wrong key?: " + e.getMessage());
            throw new SeattleException(e);
        } finally {
            return encData;
        }
    }
}
