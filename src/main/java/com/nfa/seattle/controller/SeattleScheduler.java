package com.nfa.seattle.controller;

import org.quartz.*;

import java.util.logging.Level;
import java.util.logging.Logger;

import static org.quartz.JobBuilder.*;
import static org.quartz.TriggerBuilder.*;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;

/**
 * Created by barclakj on 10/02/2018.
 */
public class SeattleScheduler {
    private static Logger log = Logger.getLogger(SeattleScheduler.class.getCanonicalName());

    private SchedulerFactory schedFact = null;
    private Scheduler sched = null;

    public SeattleScheduler() {
        super();
        try {
            schedFact = new org.quartz.impl.StdSchedulerFactory();
            sched = schedFact.getScheduler();
            sched.start();
            this.addCleanupJob();
        } catch (SchedulerException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void addCleanupJob() throws SchedulerException {
        // define the CleanupProjectsJob job
        JobDetail job = newJob(CleanupProjectsJob.class)
                .withIdentity("cleanupJob", "group1")
                .build();

        // Trigger the job to run now, and then every 30 seconds
        Trigger trigger = newTrigger()
                .withIdentity("cleanupTrigger", "group1")
                .startNow()
                .withSchedule(simpleSchedule()
                        .withIntervalInSeconds(30)
                        .repeatForever())
                .build();

        // Tell quartz to schedule the job using our trigger
        sched.scheduleJob(job, trigger);
    }

}
