package com.nfa.seattle.controller;

/**
 * Created by barclakj on 17/02/2018.
 */
public class SeattleException extends Exception {

    public SeattleException() {
        super();
    }

    public SeattleException(Throwable t) {
        super(t);
    }

    public SeattleException(String msg) {
        super(msg);
    }
}
