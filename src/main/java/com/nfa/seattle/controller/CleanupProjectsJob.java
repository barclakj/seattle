package com.nfa.seattle.controller;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.logging.Logger;

/**
 * Created by barclakj on 10/02/2018.
 */
public class CleanupProjectsJob implements Job {
    private static Logger log = Logger.getLogger(CleanupProjectsJob.class.getCanonicalName());

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.fine("Cleaning up projects...");
        SeattleClientTracker.cleanupProjects();
    }
}
