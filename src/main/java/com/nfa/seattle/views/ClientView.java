package com.nfa.seattle.views;

import com.nfa.seattle.model.Client;
import org.json.JSONObject;

/**
 * Created by barclakj on 10/02/2018.
 */
public class ClientView {

    public static JSONObject toJSON(Client c) {
        JSONObject jo = new JSONObject();
        if (c!=null) {
            jo.put("address", c.getAddress());
            jo.put("uuid", c.getUuid());
            jo.put("index", c.getId());
            jo.put("ts", c.getTs());
            jo.put("createdTs", c.getCreatedTs());
        }
        return jo;
    }

    public static Client toClient(JSONObject jo) {
        Client c = null;
        if (jo!=null) {
            c = new Client();
            if (jo.has("address")) c.setAddress(jo.getString("address"));
            if (jo.has("uuid")) c.setUuid(jo.getString("uuid"));
            if (jo.has("index")) c.setId(jo.getInt("index"));
            if (jo.has("ts")) c.setTs(jo.getLong("ts"));
            if (jo.has("createdTs")) c.setCreatedTs(jo.getLong("createdTs"));
        }
        return c;
    }
}
