package com.nfa.seattle.views;

import com.nfa.seattle.model.Client;
import com.nfa.seattle.model.Project;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by barclakj on 10/02/2018.
 */
public class ProjectView {

    public static JSONObject toJSON(Collection<Project> projects) {
        JSONObject jo = new JSONObject();
        JSONArray ja = new JSONArray();
        for(Project p : projects) {
            JSONObject j2 = toJSON(p);
            ja.put(j2);
        }
        jo.put("projects", ja);
        return jo;
    }

    public static JSONObject toJSON(Project p) {
        JSONObject jo = new JSONObject();
        if (p!=null) {
            jo.put("name", p.getProjectName());
            jo.put("clientCount", p.getClients().size());
            JSONArray ja = new JSONArray();
            for(Client c : p.getClients()) {
                ja.put( ClientView.toJSON(c) );
            }
            jo.put("clients", ja);
        }
        return jo;
    }

    public static Map<String, Project> toProjectMap(String json) {
        JSONObject jo = new JSONObject(json);
        return toProjectMap(jo);
    }

    public static Map<String, Project> toProjectMap(JSONObject jo) {
        Map<String, Project> projects = null; //new HashMap<>();
        if (jo!=null) {
            projects = new HashMap<>();
            if (jo.has("projects")) {
                JSONArray ja = jo.getJSONArray("projects");
                for(int i=0;i<ja.length();i++) {
                    JSONObject j2 = ja.getJSONObject(i);
                    Project p = toProject(j2);
                    if (p!=null) projects.put(p.getProjectName(), p);
                }
            }
        }
        return projects;
    }

    public static Project toProject(JSONObject jo) {
        Project p = null;
        if (jo!=null) {
            p = new Project();
            if (jo.has("name")) p.setProjectName(jo.getString("name"));
            if (jo.has("clients")) {
                JSONArray ja = jo.getJSONArray("clients");
                for(int i=0;i<ja.length();i++) {
                    JSONObject j2 = ja.getJSONObject(i);
                    Client c = ClientView.toClient(j2);
                    if (c!=null) p.addClient(c.getUuid(), c);
                }
            }
       }
       return p;
    }
}
