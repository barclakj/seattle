package com.nfa.seattle.model;

import org.json.JSONObject;

import java.util.*;

/**
 * Project model.
 * Created by barclakj on 10/02/2018.
 */
public class Project {
    /**
     * Mape of client uuids to clients.
     */
    private Map<String, Client> clientMap = new HashMap<>();
    /**
     * Project name.
     */
    private String projectName;
    /**
     * Config data.
     */
    private byte[] configData;
    /**
     * Last time the data was loaded.
     */
    private long lastLoadTs = 0;

    /**
     * Adds client to the set.
     * @param uuid
     * @param c
     */
    public void addClient(String uuid, Client c) {
        clientMap.put(uuid, c);
    }

    /**
     * Removes client by UUID from the map.
     * @param uuid
     */
    public void removeClient(String uuid) {
        clientMap.remove(uuid);
    }

    /**
     * Returns a collection of clients.
     * @return
     */
    public Collection<Client> getClients() {
        return clientMap.values();
    };

    /**
     * Returns the client details by uuid.
     * @param uuid
     * @return
     */
    public Client getClient(String uuid) {
        return clientMap.get(uuid);
    }

    /**
     * Updates the client to notify that we've seen this one.
     * @param uuid
     */
    public void ping(String uuid) {
        Client c = getClient(uuid);
        if (c!=null) c.setTs( System.currentTimeMillis() );
    }

    /**
     * Locates the lowest missing value from the current list of
     * clients.
     * @return
     */
    public int getNextId() {
        int id = 0;
        boolean found;

        do {
            found = false;
            for (Client c : clientMap.values()) {
                if (c.getId()==id) {
                    id+=1;
                    found = true;
                    break;
                }
            }
        } while(found);

        return id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public byte[] getConfigData() {
        return configData;
    }

    public void setConfigData(byte[] configData) {
        this.configData = configData;
    }

    public long getLastLoadTs() {
        return lastLoadTs;
    }

    public void setLastLoadTs(long lastLoadTs) {
        this.lastLoadTs = lastLoadTs;
    }
}
