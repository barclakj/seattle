package com.nfa.seattle.model;

/**
 * Client model.
 * Created by barclakj on 10/02/2018.
 */
public class Client {
    /**
     * Unique name for the client.
     */
    private String uuid = null;
    /**
     * Address for the client. Either name or IP.
     */
    private String address = null;
    /**
     * Sequential ID of the client.
     */
    private int id = -1;
    /**
     * Timestamp as long when the client was last contacted.
     */
    private long ts = 0;

    /**
     * When created.
     */
    private long createdTs = 0;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getTs() {
        return ts;
    }

    public void setTs(long ts) {
        this.ts = ts;
    }

    public long getCreatedTs() {
        return createdTs;
    }

    public void setCreatedTs(long createdTs) {
        this.createdTs = createdTs;
    }
}
